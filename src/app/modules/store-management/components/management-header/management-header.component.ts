import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-management-header',
  template: `
    <flexbox alignItems="center" justifyContent="space-between">
<!--      <flexbox alignItems="center">-->
<!--        <button class="bg-light text-black-50" mat-fab><mat-icon>add</mat-icon></button>-->
<!--        <span class="ml-3 text-black-50">Add user</span>-->
<!--      </flexbox>-->
      <span></span>
      <button class="py-2 px-4 text-black-50" mat-stroked-button>
        Save changes
        <mat-icon class="ml-3">check_circle_outline</mat-icon>
      </button>
    </flexbox>
  `,
  styleUrls: ['./management-header.component.scss'],
})
export class ManagementHeaderComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
