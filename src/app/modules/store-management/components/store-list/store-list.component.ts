import {Component, OnInit} from '@angular/core';
import {StoreService} from '../../../../services/store.service';
import {StoreModel} from '../../models/store.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-store-list',
  template: `
    <div>
      <header class="card-header_wrapper"></header>
      <mat-card class="card-wrapper bg-transparent mat-elevation-z0">
        <p class="text-black-50 mb-0 ml-3">
          Нераспределенные магазины: {{ (stores | async).length }}
          <button class="cdk-program-focused ml-2" mat-icon-button color="accent"><mat-icon>warning</mat-icon></button>
        </p>
        <mat-list>
          <mat-list-item *ngFor="let store of stores | async">
            <mat-icon (click)="assignStore(store)" class="pointer" matRipple matListIcon>keyboard_backspace</mat-icon>
            <img class="ml-2" [src]="store.storeLogo" [alt]="" matListAvatar />
            <span matLine>
              {{ store.storeName }} <br />
              <small class="text-black-50">{{ store.storeAddress }}</small>
            </span>
          </mat-list-item>
        </mat-list>
      </mat-card>
    </div>
  `,
  styleUrls: ['./store-list.component.scss'],
})
export class StoreListComponent implements OnInit {
  stores: Observable<StoreModel[]>;
  constructor(private storeService: StoreService) {}

  ngOnInit(): void {
    this.stores = this.storeService.stores.pipe(map(store => store.filter(s => !s.distributedUserId)));
  }

  assignStore(store: StoreModel) {
    this.storeService.assignStoreToUser(store);
  }
}
