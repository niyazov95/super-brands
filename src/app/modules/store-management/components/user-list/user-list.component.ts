import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../../services/user.service';
import {Observable} from 'rxjs';
import {UserModel} from '../../models/user.model';
import {StoreModel} from '../../models/store.model';
import {StoreService} from '../../../../services/store.service';

@Component({
  selector: 'app-user-list',
  template: `
    <div>
      <header class="card-header_wrapper">
        <ul class="tab">
          <li
            [matBadge]="user?.stores.length ? user.stores?.length.toString() : null"
            matBadgePosition="above after"
            (click)="userService.selectedUserId = user.id"
            [ngClass]="{'isActive mat-elevation-z6': userService.selectedUserId === user.id}"
            class="tab-item"
            *ngFor="let user of users | async"
          >
            <img class="tab-item__logo" [src]="user.logo" [alt]="user.fullName" />
          </li>
          <li class="tab-item">
            <flexbox alignItems="center">
              <button (click)="userService.selectedUserId = 0" class="tab-item__create text-black-50" mat-mini-fab>
                <mat-icon>add</mat-icon>
              </button>
              <span class="ml-2" *ngIf="!(users | async)?.length">Добавить специалиста</span>
            </flexbox>
          </li>
        </ul>
      </header>
      <mat-card class="mat-elevation-z6 card-wrapper" [ngSwitch]="userService.selectedUserId">
        <ng-container *ngFor="let user of users | async">
          <ng-container *ngSwitchCase="user.id">
            <div>
              <div class="my-3">
                <p class="text-black-50 mb-0 ml-3">Специалист</p>
                <mat-list>
                  <mat-list-item>
                    <img matListAvatar [src]="user.logo" [alt]="user.fullName" />
                    <span class="ml-2" matline>
                      {{ user.fullName }}
                      <a class="text-primary ml-2">{{ user.stores ? user.stores.length : 0 }} магазинов</a>
                    </span>
                    <button (click)="userService.removeUser(user)" class="ml-auto" mat-icon-button>
                      <mat-icon class="text-black-50">delete</mat-icon>
                    </button>
                  </mat-list-item>
                </mat-list>
              </div>
              <div class="my-3">
                <p class="text-black-50 mb-0 ml-3">Магазины</p>
                <p *ngIf="!user.stores?.length; else stores" class="ml-3">
                  Не назначены
                  <button class="cdk-program-focused ml-2" mat-icon-button color="accent"><mat-icon>warning</mat-icon></button>
                </p>
                <ng-template #stores>
                  <mat-list>
                    <mat-list-item *ngFor="let store of user.stores">
                      <img class="ml-2" [src]="store.storeLogo" [alt]="" matListAvatar />
                      <span matLine>
                        {{ store.storeName }} <br />
                        <small class="text-black-50">{{ store.storeAddress }}</small>
                      </span>
                      <button (click)="unAssignStore(store)" mat-icon-button class="ml-auto">
                        <mat-icon class="text-black-50">remove_circle_outline</mat-icon>
                      </button>
                    </mat-list-item>
                  </mat-list>
                </ng-template>
              </div>
            </div>
          </ng-container>
        </ng-container>
        <ng-container *ngSwitchCase="0">
          <!--          <form>-->
          <mat-form-field class="w-100">
            <mat-label>Add user</mat-label>
            <input [(ngModel)]="userName" placeholder="user name" type="text" matInput />
            <button (click)="userService.addUser(userName)" mat-icon-button matSuffix><mat-icon>add</mat-icon></button>
          </mat-form-field>
          <!--          </form>-->
        </ng-container>
        <ng-container *ngSwitchCase="null">
          <!--          <div >-->
          <flexbox class="h-100" flexDirection="column" alignItems="center" justifyContent="center">
            <h3 class="ls-2px">Специалист не назначен</h3>
            <p class="text-black-50 w-50">Чтобы начать работу с нераспределенными магазинами, вам необходимо добавить хотя бы одного специалиста</p>
            <mat-icon class="text-black-50 icon-lg">person_add</mat-icon>
          </flexbox>
          <!--          </div>-->
        </ng-container>
      </mat-card>
    </div>
  `,
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  users: Observable<UserModel[]>;
  userName: string;
  constructor(public userService: UserService, private storeService: StoreService) {}

  ngOnInit(): void {
    this.users = this.userService.users;
  }

  unAssignStore(store: StoreModel) {
    this.storeService.unAssignStoreFromUser(store);
  }
}
