import {UserModel} from '../models/user.model';

export const USER_LIST: UserModel[] = [
  {
    id: 1,
    fullName: 'Аношкин Станислав Касьянович',
    logo: 'assets/images/mock/alex.png',
    stores: [],
  },
  {
    id: 2,
    fullName: 'Бажанов Вячеслав Ираклиевич',
    logo: 'assets/images/mock/alex.png',
    stores: [],
  },
  {
    id: 3,
    fullName: 'Игумнов Демьян Эмилевич',
    logo: 'assets/images/mock/alex.png',
    stores: [],
  },
  {
    id: 4,
    fullName: 'Качусов Вячеслав Евстафиевич',
    logo: 'assets/images/mock/alex.png',
    stores: [],
  },
  {
    id: 5,
    fullName: 'Ананьев Дмитрий Евстафиевич',
    logo: 'assets/images/mock/alex.png',
    stores: [],
  },
  {
    id: 6,
    fullName: 'Соколова Полина Ильевна',
    logo: 'assets/images/mock/alex.png',
    stores: [],
  },
  {
    id: 7,
    fullName: 'Малыхин Клавдий Святославович',
    logo: 'assets/images/mock/alex.png',
    stores: [],
  },
];
