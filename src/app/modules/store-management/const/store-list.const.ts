import {StoreModel} from '../models/store.model';

export const STORE_LIST: StoreModel[] = [
  {id: 1, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Москва', storeAddress: 'Россия, г. Москва, Бутырская 77'},
  {
    id: 2,
    storeLogo: 'assets/images/mock/store_logo.png',
    storeName: 'Магазин SBS Санкт-Петербург',
    storeAddress: 'Россия, г. Санкт-Петербург, Бутырская 77',
  },
  {id: 3, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Томск', storeAddress: 'Россия, г. Томск, Бутырская 77'},
  {id: 4, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Коломна', storeAddress: 'Россия, г. Коломна, Бутырская 77'},
  {id: 5, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Тверь', storeAddress: 'Россия, г. Тверь, Бутырская 77'},
  {id: 6, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Калуга', storeAddress: 'Россия, г. Калуга, Бутырская 77'},
  {id: 7, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Сыктывкар', storeAddress: 'Россия, г. Сыктывкар, Бутырская 77'},
  {id: 8, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Сочи', storeAddress: 'Россия, г. Сочи, Бутырская 77'},
  {id: 9, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Астана', storeAddress: 'Казахстан, г. Астана, Бутырская 77'},
  {id: 10, storeLogo: 'assets/images/mock/store_logo.png', storeName: 'Магазин SBS Минск', storeAddress: 'Беларусь, г. Минск, Бутырская 77'},
];
