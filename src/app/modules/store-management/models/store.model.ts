export class StoreModel {
  id: number;
  storeName: string;
  storeAddress: string;
  storeLogo: string;
  distributedUserId?: number;
}
