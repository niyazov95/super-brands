import {StoreModel} from './store.model';

export class UserModel {
  id: number;
  fullName: string;
  logo: string;
  stores?: StoreModel[];

  constructor(name, id) {
    this.fullName = name;
    this.id = id;
    this.logo = 'assets/images/mock/alex.png';
    this.stores = [];
  }
}
