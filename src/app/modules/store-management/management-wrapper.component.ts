import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-management-wrapper',
  template: `
    <mat-card class="container-fluid my-5 bg-light">
      <section class="row">
        <header class="col-12">
          <app-management-header></app-management-header>
        </header>
        <main class="col-12">
          <div class="row">
            <div class="col-lg-6">
              <app-user-list></app-user-list>
            </div>
            <div class="col-lg-6">
              <app-store-list></app-store-list>
            </div>
          </div>
        </main>
      </section>
    </mat-card>
  `,
  styles: [],
})
export class ManagementWrapperComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
