import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ManagementWrapperComponent} from './management-wrapper.component';

const routes: Routes = [
  {
    path: '',
    component: ManagementWrapperComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StoreManagementRoutingModule {}
