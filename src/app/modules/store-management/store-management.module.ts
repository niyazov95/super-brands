import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {StoreManagementRoutingModule} from './store-management-routing.module';
import {ManagementWrapperComponent} from './management-wrapper.component';
import {UserListComponent} from './components/user-list/user-list.component';
import {StoreListComponent} from './components/store-list/store-list.component';
import {ManagementHeaderComponent} from './components/management-header/management-header.component';
import {SharedModule} from '../shared/shared.module';
import {MaterialModule} from '../material/material.module';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [ManagementWrapperComponent, UserListComponent, StoreListComponent, ManagementHeaderComponent],
  imports: [CommonModule, StoreManagementRoutingModule, SharedModule, MaterialModule, FormsModule],
})
export class StoreManagementModule {}
