import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {loadStoreManagement} from './lazy-load';

const routes: Routes = [
  {
    path: '**',
    redirectTo: 'management',
    pathMatch: 'full',
  },
  {
    path: 'management',
    loadChildren: loadStoreManagement,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
