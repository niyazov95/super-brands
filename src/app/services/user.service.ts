import {EventEmitter, Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {UserModel} from '../modules/store-management/models/user.model';
import {USER_LIST} from '../modules/store-management/const/user-list.const';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public users: BehaviorSubject<UserModel[]> = new BehaviorSubject<UserModel[]>(USER_LIST);
  public selectedUserId: number = 1;
  $triggerUserRemove: EventEmitter<UserModel> = new EventEmitter<UserModel>();

  constructor() {}

  addUser(userName: string) {
    const users = this.users.getValue();
    const user = new UserModel(userName, users?.length + 1);
    this.users.next([...users, user]);
    this.selectedUserId = user.id;
  }

  removeUser(user: UserModel) {
    const users = this.users.getValue();
    users.splice(users.indexOf(user), 1);
    this.users.next(users);
    !users.length ? (this.selectedUserId = null) : (this.selectedUserId = users[0].id);
    this.$triggerUserRemove.emit(user);
  }
}
