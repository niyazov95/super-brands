import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {STORE_LIST} from '../modules/store-management/const/store-list.const';
import {StoreModel} from '../modules/store-management/models/store.model';
import {UserService} from './user.service';
import {UserModel} from '../modules/store-management/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  public stores: BehaviorSubject<StoreModel[]> = new BehaviorSubject<StoreModel[]>(STORE_LIST);

  constructor(private userService: UserService) {
    this.userService.$triggerUserRemove.subscribe((user: UserModel) => {
      this.unAssignStoresFromUser(user.stores);
    });
  }

  assignStoreToUser(store: StoreModel) {
    const users = this.userService.users.getValue();
    const currentUser = users.find(u => u.id === this.userService.selectedUserId);
    const stores = this.stores.getValue();
    const currentStore = stores.find(s => s.id === store.id);
    if (currentUser) {
      currentUser.stores.push(store);
      currentStore.distributedUserId = currentUser.id;
      this.stores.next(stores);
    }
  }

  unAssignStoreFromUser(store: StoreModel) {
    const users = this.userService.users.getValue();
    const currentUser = users.find(u => u.id === this.userService.selectedUserId);
    const index = currentUser.stores.indexOf(store);
    currentUser.stores.splice(index, 1);
    const stores = this.stores.getValue();
    const currentStore = stores.find(s => s.id === store.id);
    currentStore.distributedUserId = null;
    this.stores.next(stores);
  }
  unAssignStoresFromUser(assignedStores: StoreModel[]) {
    const stores = this.stores.getValue();
    stores.forEach(s => {
      assignedStores.forEach(aS => {
        if (s.id === aS.id) {
          s.distributedUserId = null;
        }
      });
    });
    this.stores.next(stores);
  }
}
